export default function checkEnvironment() {
  ["FIRESTORE_PROJECT_ID", "FIRESTORE_KEY_FILE_PATH"].forEach((val) => {
    if (!process.env[val]) {
      console.error(`The environment variable ${val} is not set!`);
      process.exit(2);
    }
  });
}
