#! /usr/bin/env node

import { Firestore } from "@google-cloud/firestore";
import getTutors from "./getTutors";
import checkEnvironment from "./checkEnvironment";

checkEnvironment();

const db = new Firestore({
  projectId: process.env.FIRESTORE_PROJECT_ID,
  keyFilename: process.env.FIRESTORE_KEY_FILE_PATH
});

const batch = db.batch();
for (const tutor of getTutors()) {
  const doc = db.collection("tutors").doc(`${tutor.first}.${tutor.last}`);
  batch.set(doc, tutor.asObject);
}

batch.commit().then(result => {
  console.log(result);
});
